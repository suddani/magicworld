#ifndef MENU_H
#define MENU_H

#include <core/IProcess.h>
#include <core/ConsoleParameter.h>
namespace irr
{
    namespace gui
    {
        class IGUIEnvironment;
        class IGUIButton;
    }
    namespace video
    {
        class IVideoDriver;
    }
}
class Menu : public AppFrame::IProcess
{
    public:
        Menu(const AppFrame::SCommand& command);
        virtual ~Menu();

        //IProcess
        virtual void Update(void);

        virtual bool OnEvent(const irr::SEvent::SGUIEvent& event);
        virtual void onAttach(void);
		virtual void onDetach(void);
        virtual void makeMain(void);
        virtual void makeSlave(void);
        virtual const AppFrame::c8* getProcessName(void){return "MainMenu";}
    protected:
        irr::gui::IGUIEnvironment* gui;
        irr::gui::IGUIButton* play;
        irr::gui::IGUIButton* editor;
        irr::gui::IGUIButton* exit;

        irr::video::IVideoDriver* driver;
    private:
};

#endif // MENU_H
