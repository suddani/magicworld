#include "Menu.h"
#include <core/IProcessManager.h>
#include <core/IGraphicManager.h>
#include <core/IConsole.h>
#include <IGUIEnvironment.h>

Menu::Menu(const AppFrame::SCommand& command)
{
    //ctor
}

Menu::~Menu()
{
    //dtor
}

void Menu::Update(void)
{

}

bool Menu::OnEvent(const irr::SEvent::SGUIEvent& event)
{
    if (event.Caller == exit && event.EventType == irr::gui::EGET_BUTTON_CLICKED) {
        Manager->killActiveProcess();
        return true;
    }
    else if (event.Caller == play && event.EventType == irr::gui::EGET_BUTTON_CLICKED) {
        AppFrame::IProcess* p = Manager->createProcess("mtst");
        //Manager->getConsole()->ExecuteCommand("/bindkey escape kill "+p->ProcessID());
        Manager->setMainProcess(p->getProcessID());
        return true;
    }
    return false;
}

void Menu::onAttach(void)
{
    gui = Manager->getGraphicManager()->getDevice()->getGUIEnvironment();
    driver = gui->getVideoDriver();
}

void Menu::onDetach(void)
{

}

void Menu::makeMain(void)
{
    irr::core::dimension2d<irr::u32> dim = driver->getScreenSize();
    play = gui->addButton(irr::core::rect<irr::s32>(dim.Width/2-60,100,dim.Width/2+60,150), 0, -1, L"Play");
    editor = gui->addButton(irr::core::rect<irr::s32>(dim.Width/2-60,160,dim.Width/2+60,210), 0, -1, L"Editor");
    exit = gui->addButton(irr::core::rect<irr::s32>(dim.Width/2-60,220,dim.Width/2+60,270), 0, -1, L"Exit");
}

void Menu::makeSlave(void)
{
    play->remove();
    exit->remove();
    editor->remove();
}
