#ifndef CUBETEST_H
#define CUBETEST_H

#include <core/IProcess.h>
#include <core/ConsoleParameter.h>

#include "../cubes/DotChunk.h"

namespace irr
{
    namespace gui
    {
        class IGUIEnvironment;
        class IGUIButton;
    }
    namespace video
    {
        class IVideoDriver;
    }
}
class Input;
class Cubetest : public AppFrame::IProcess
{
    public:
        Cubetest(const AppFrame::SCommand& command);
        virtual ~Cubetest();

        //IProcess
        virtual void Update(void);

        virtual bool OnEvent(const irr::SEvent::SGUIEvent& event);
        virtual void onAttach(void);
		virtual void onDetach(void);
        virtual void makeMain(void);
        virtual void makeSlave(void);
        virtual const AppFrame::c8* getProcessName(void){return "MarchingCubesTest";}




        void updateMesh();
        int cubes;
    protected:
        DotChunk dotchunk[25];
        DotChunk dotchunk_cubes[25];
        irr::scene::IMeshBuffer* chunkBuffers[25];
        irr::scene::IMeshBuffer* chunkBuffers_cubes[25];
        irr::scene::IMeshSceneNode* worldnode;
        irr::scene::ICameraSceneNode* camera;

        Input* input;

        void showFPS(const AppFrame::SCommand& cmd);
    private:
};

#endif // CUBETEST_H
