#include "Cubetest.h"
#include <core/IProcessManager.h>
#include <core/IGraphicManager.h>
#include <core/IConsole.h>
#include <IGUIEnvironment.h>

#include "../cubes/generator/MarchingCube.h"
#include "../cubes/generator/MineCubes.h"
#include "../cubes/generator/MarchingTetrahedron.h"
#include "../cubes/Dot.h"
#include <core/InputListener.h>
class Input : public AppFrame::input::IMouseListener, public AppFrame::input::IKeyListener
{
public:
    Input(irr::scene::ICameraSceneNode* camera, DotChunk* chunk, DotChunk* chunk_cubes, Cubetest* cube)
    {
        this->camera = camera;
        this->chunk = chunk;
        this->chunk_cubes = chunk_cubes;
        this->cube = cube;
        this->drawSize = 0.2f;
    }
    void key_event(const irr::EKEY_CODE& key, const AppFrame::input::KeyState& state)
    {
        if (key == irr::KEY_KEY_Q && state.pressed == AppFrame::input::EKS_UP)
        {
            cube->cubes = (cube->cubes+1)%2;
            //for (int i=0;i<25;i++) chunk[i].setChanged(true);
            //cube->updateMesh();
        }
    }
    void mouse_event(const AppFrame::input::E_MOUSE_EVENT_TYPE& type, const AppFrame::input::MouseState& state)
    {
        if (type == AppFrame::input::EMET_LEFT && state.LeftPressed == AppFrame::input::EKS_UP)
        {
            irr::core::vector3df dir = camera->getTarget()-camera->getPosition();
            dir.normalize();
            bool changed = false;
            for (int i=0;i<25;i++)
            {
                changed = chunk[i].remove(camera->getPosition()+dir*2.f, drawSize) ? true : changed;
                changed = chunk_cubes[i].remove(camera->getPosition()+dir*2.f, drawSize) ? true : changed;
            }
            if (changed) cube->updateMesh();
        }
        else if (type == AppFrame::input::EMET_RIGHT && state.RightPressed == AppFrame::input::EKS_UP)
        {
            irr::core::vector3df dir = camera->getTarget()-camera->getPosition();
            dir.normalize();
            bool changed = false;
            for (int i=0;i<25;i++)
            {
                changed = chunk[i].change(camera->getPosition()+dir*2.f, drawSize, cube->cubes == 0) ? true : changed;
                changed = chunk_cubes[i].change(camera->getPosition()+dir*2.f, drawSize, cube->cubes != 0) ? true : changed;
            }
            if (changed) cube->updateMesh();
        }else if (type == AppFrame::input::EMET_WHEEL)
        {
            drawSize += state.MouseWheel/10.f;
        }
    }
protected:
    irr::scene::ICameraSceneNode* camera;
    DotChunk* chunk;
    DotChunk* chunk_cubes;
    Cubetest* cube;
    irr::f32 drawSize;
};

Cubetest::Cubetest(const AppFrame::SCommand& command)
{
    //ctor
    worldnode = NULL;
    camera = NULL;
    input = NULL;
    cubes = 0;
    for(int i=0;i<25;i++)
        chunkBuffers[i] = NULL;
}

Cubetest::~Cubetest()
{
    for(int i=0;i<25;i++)
        if (chunkBuffers[i] != NULL) chunkBuffers[i]->drop();
}

void Cubetest::Update(void)
{

}

bool Cubetest::OnEvent(const irr::SEvent::SGUIEvent& event)
{
    return false;
}

void Cubetest::onAttach(void)
{
    float size = 0.25;
//    for(int a=0;a<10;a++)
//        for(int b=0;b<10;b++)
//            for(int c=0;c<10;c++)
//                Manager->getGraphicManager()->getDevice()->getSceneManager()->addSphereSceneNode(0.1)->setPosition(irr::core::vector3df(a*size,b*size,c*size));
    for(int x=0;x<5;x++)
        for(int y=0;y<5;y++)
        {
            dotchunk[y+5*x].generateDots(16,256,16,size,irr::core::vector3df(15*x*size,0,y*15*size), true);
            dotchunk_cubes[y+5*x].generateDots(40,50,40,size,irr::core::vector3df(15*x*size,0,y*15*size), false);
            //dotchunk[y+5*x].getDot(1,1,1)->setActive(true);
            //dotchunk[y+2*x].getDot(1,2,1)->setActive(true);
        }
}

void Cubetest::onDetach(void)
{
}

void Cubetest::makeMain(void)
{
    camera = Manager->getGraphicManager()->getDevice()->getSceneManager()->addCameraSceneNodeFPS(0,100,0.01);
    camera->setNearValue(0.01);
    camera->grab();
    Manager->getConsole()->ExecuteCommand("/bindkey escape exit");

    input = new Input(camera, dotchunk, dotchunk_cubes, this);
    Manager->getInputManager()->addListener((AppFrame::input::IMouseListener*)input);
    Manager->getInputManager()->addListener((AppFrame::input::IKeyListener*)input);

    worldnode = Manager->getGraphicManager()->getDevice()->getSceneManager()->addMeshSceneNode(NULL, NULL, -1, irr::core::vector3df(0,0,0), irr::core::vector3df(0,0,0), irr::core::vector3df(1,1,1), true);
    //worldnode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    //worldnode->setMaterialFlag(irr::video::EMF_BACK_FACE_CULLING, false);
    //worldnode->setMaterialFlag(irr::video::EMF_WIREFRAME, true);
    worldnode->setAutomaticCulling(irr::scene::EAC_OFF);
    worldnode->grab();

    Manager->getGraphicManager()->getDevice()->getSceneManager()->addLightSceneNode(worldnode, irr::core::vector3df(55,55,55), irr::video::SColorf(0.4f, 0.4f, 0.4f), 50);
    Manager->getGraphicManager()->getDevice()->getSceneManager()->addCubeSceneNode(10, worldnode)->setPosition(irr::core::vector3df(55,55,55));
    Manager->getGraphicManager()->getDevice()->getSceneManager()->addCubeSceneNode(0.1, camera, -1, irr::core::vector3df(0,0,2));
    Manager->getGraphicManager()->getDevice()->getSceneManager()->setAmbientLight(irr::video::SColorf(0.5,0.5,0.5,1.0));

    Manager->getConsole()->RegisterConsoleCommand("/fps", CONSOLE_CALLBACK_METHOD(Cubetest, showFPS), "show fps");
    Manager->getConsole()->ExecuteCommand("/bindkey f fps");


    updateMesh();
}

void Cubetest::showFPS(const AppFrame::SCommand& cmd)
{
    nlog<<"FPS: "<<Manager->getGraphicManager()->getDevice()->getVideoDriver()->getFPS()<<nlendl;
}

void Cubetest::makeSlave(void)
{
    worldnode->remove();
    worldnode->drop();
    worldnode = NULL;

    camera->remove();
    camera->drop();
    camera = NULL;
    Manager->getConsole()->unRegisterConsoleCommand("/fps", this);
    Manager->getConsole()->ExecuteCommand("/unbindkey escape");
    Manager->getConsole()->ExecuteCommand("/unbindkey f");
    Manager->getInputManager()->removeListener((AppFrame::input::IMouseListener*)input);
    Manager->getInputManager()->removeListener((AppFrame::input::IKeyListener*)input);
    delete input;
}

void Cubetest::updateMesh()
{
    MarchingTetrahedron tcube;
    MineCubes ccube;
    MarchingCube mcube;

    //IMeshGenerator* cube = (cubes == 0 ? (IMeshGenerator*)&ccube : cubes == 1 ? (IMeshGenerator*)&mcube : (IMeshGenerator*)&tcube);

    irr::scene::SMesh* mesh = new irr::scene::SMesh();
    for (int i=0;i<25;i++)
    {
        if (dotchunk[i].isChanged())
        {
            if (chunkBuffers[i] != NULL) chunkBuffers[i]->drop();
            chunkBuffers[i] = mcube.march(dotchunk[i]);
            mesh->addMeshBuffer(chunkBuffers[i]);
            dotchunk[i].setChanged(false);
        }
        else {
            mesh->addMeshBuffer(chunkBuffers[i]);
        }

        if (dotchunk_cubes[i].isChanged())
        {
            if (chunkBuffers_cubes[i] != NULL) chunkBuffers_cubes[i]->drop();
            chunkBuffers_cubes[i] = ccube.march(dotchunk_cubes[i]);
            mesh->addMeshBuffer(chunkBuffers_cubes[i]);
            dotchunk_cubes[i].setChanged(false);
        }
        else {
            mesh->addMeshBuffer(chunkBuffers_cubes[i]);
        }

        mesh->recalculateBoundingBox();
    }
    if (worldnode == NULL)
    {
        nlerror<<"WTF"<<nlendl;
        //worldnode = Manager->getGraphicManager()->getDevice()->getSceneManager()->addMeshSceneNode(mesh);

        //worldnode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
        //worldnode->setMaterialFlag(irr::video::EMF_BACK_FACE_CULLING, false);
        //worldnode->setMaterialFlag(irr::video::EMF_WIREFRAME, true);
        //worldnode->setAutomaticCulling(irr::scene::EAC_OFF);
        //worldnode->grab();
    }
    else
        worldnode->setMesh(mesh);

    worldnode->setMaterialTexture(0, Manager->getGraphicManager()->getDevice()->getVideoDriver()->getTexture("textures/basic.png"));
    mesh->drop();


}
