#include "MagicWorld.h"
#include "process/Menu.h"
#include "process/Cubetest.h"

MagicWorld::MagicWorld(int argc, char* argv[]) : CApplication(argc, argv)
{
    //ctor
}

MagicWorld::~MagicWorld()
{
    //dtor
}

void MagicWorld::registerProcess(void)
{
    //AppFrame::ILog::addReceiver(getManager()->getConsole());
    this->getManager()->registerProcess<Menu>(AppFrame::ProcessID("menu"));
    this->getManager()->registerProcess<Cubetest>(AppFrame::ProcessID("mtst"));
    this->getManager()->createProcess("menu");
}
