#ifndef DOT_H
#define DOT_H

#include <vector3d.h>
#include <core/IRef.h>
class DotChunk;
class Dot : public AppFrame::IRef
{
    public:
        Dot();
        Dot(const irr::core::vector3df& pos, const irr::f32& size, int x, int y, int z, DotChunk* chunk);
        virtual ~Dot();

        void init(const irr::core::vector3df& pos, const irr::f32& size, int x, int y, int z, DotChunk* chunk);
        void init(const irr::core::vector3df& pos, const irr::f32& size, const bool& active, int x, int y, int z, DotChunk* chunk);

        void setActive(const bool& active);
        const bool& isActive() const;
        const irr::core::vector3df& getPosition() const;
        irr::core::vector3df getNormal() const;
        const irr::f32& getSize() const;
    protected:
        bool active;
        irr::core::vector3df pos;
        irr::f32 size;

        int x;
        int y;
        int z;
        DotChunk* chunk;
    private:
};

#endif // DOT_H
