#ifndef MARCHINGTETRAHEDRON_H
#define MARCHINGTETRAHEDRON_H

#include "IMeshGenerator.h"
class MarchingTetrahedron : public IMeshGenerator
{
    public:
        MarchingTetrahedron();
        virtual ~MarchingTetrahedron();
        irr::scene::SMeshBuffer* march(DotChunk& chunk);
    protected:
        void marchCube(Dot* cube[8]);
        void marchTetrahedron(Dot* tetra[4]);
        irr::scene::SMeshBuffer* buffer;
        irr::f32 distance;
        int triangleCount;
        int tetraCount;
    private:
};

#endif // MARCHINGTETRAHEDRON_H
