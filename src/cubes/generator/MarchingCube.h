#ifndef MARCHINGCUBE_H
#define MARCHINGCUBE_H

#include "IMeshGenerator.h"
class MarchingCube : public IMeshGenerator
{
    public:
        MarchingCube();
        virtual ~MarchingCube();
        irr::scene::SMeshBuffer* march(DotChunk& chunk);
    protected:
        void marchCube(Dot* cube[8], irr::core::vector3df pos);
        irr::core::vector2d<irr::f32> getTexCoords(const irr::core::vector3df& pos, const irr::core::vector3df& normal);
        irr::scene::SMeshBuffer* buffer;
        irr::f32 distance;
        int triangleCount;
        int tetraCount;
    private:
};

#endif // MARCHINGCUBE_H
