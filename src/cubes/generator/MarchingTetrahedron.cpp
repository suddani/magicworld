#include "MarchingTetrahedron.h"
#include "../DotChunk.h"
#include "../Dot.h"

static const irr::f32 a2fVertexOffset[8][3] =
{
    {0.0, 0.0, 0.0},{1.0, 0.0, 0.0},{1.0, 1.0, 0.0},{0.0, 1.0, 0.0},
    {0.0, 0.0, 1.0},{1.0, 0.0, 1.0},{1.0, 1.0, 1.0},{0.0, 1.0, 1.0}
};

static const irr::s32 a2iVertexOffset[8][3] =
{
    {0, 0, 0},{1, 0, 0},{1, 1, 0},{0, 1, 0},
    {0, 0, 1},{1, 0, 1},{1, 1, 1},{0, 1, 1}
};

//a2iEdgeConnection lists the index of the endpoint vertices for each of the 12 edges of the cube
static const irr::s32 a2iEdgeConnection[12][2] =
{
    {0,1}, {1,2}, {2,3}, {3,0},
    {4,5}, {5,6}, {6,7}, {7,4},
    {0,4}, {1,5}, {2,6}, {3,7}
};

//a2fEdgeDirection lists the direction vector (vertex1-vertex0) for each edge in the cube
static const irr::f32 a2fEdgeDirection[12][3] =
{
    {1.0, 0.0, 0.0},{0.0, 1.0, 0.0},{-1.0, 0.0, 0.0},{0.0, -1.0, 0.0},
    {1.0, 0.0, 0.0},{0.0, 1.0, 0.0},{-1.0, 0.0, 0.0},{0.0, -1.0, 0.0},
    {0.0, 0.0, 1.0},{0.0, 0.0, 1.0},{ 0.0, 0.0, 1.0},{0.0,  0.0, 1.0}
};

//a2iTetrahedronEdgeConnection lists the index of the endpoint vertices for each of the 6 edges of the tetrahedron
static const irr::s32 a2iTetrahedronEdgeConnection[6][2] =
{
    {0,1},  {1,2},  {2,0},  {0,3},  {1,3},  {2,3}
};

//a2iTetrahedronEdgeConnection lists the index of verticies from a cube
// that made up each of the six tetrahedrons within the cube
static const irr::s32 a2iTetrahedronsInACube[6][4] =
{
    {0,5,1,6},
    {0,1,2,6},
    {0,2,3,6},
    {0,3,7,6},
    {0,7,4,6},
    {0,4,5,6},
};

                     /*{0, 7, 3, 2},
                     {0, 7, 2, 6},
                     {0, 4, 6, 7},
                     {0, 6, 1, 2},
                     {0, 6, 1, 4},
                     {5, 6, 1, 4}
                     };*/

irr::s32 aiTetrahedronEdgeFlags[16]=
{
    0x00, 0x0d, 0x13, 0x1e, 0x26, 0x2b, 0x35, 0x38, 0x38, 0x35, 0x2b, 0x26, 0x1e, 0x13, 0x0d, 0x00,
};

// For each of the possible vertex states listed in aiTetrahedronEdgeFlags there is a specific triangulation
// of the edge intersection points.  a2iTetrahedronTriangles lists all of them in the form of
// 0-2 edge triples with the list terminated by the invalid value -1.
//
// I generated this table by hand

irr::s32 a2iTetrahedronTriangles[16][7] =
{
    {-1, -1, -1, -1, -1, -1, -1},//
    { 0,  3,  2, -1, -1, -1, -1},//{ 0,  3,  2, -1, -1, -1, -1},
    { 0,  1,  4, -1, -1, -1, -1},
    { 1,  4,  2,  2,  4,  3, -1},

    { 1,  2,  5, -1, -1, -1, -1},
    { 0,  3,  5,  0,  5,  1, -1},
    { 0,  2,  5,  0,  5,  4, -1},
    { 5,  4,  3, -1, -1, -1, -1},

    { 3,  4,  5, -1, -1, -1, -1},
    { 4,  5,  0,  5,  2,  0, -1},
    { 1,  5,  0,  5,  3,  0, -1},
    { 5,  2,  1, -1, -1, -1, -1},

    { 3,  4,  2,  2,  4,  1, -1},
    { 4,  1,  0, -1, -1, -1, -1},
    { 2,  3,  0, -1, -1, -1, -1},
    {-1, -1, -1, -1, -1, -1, -1},
};

MarchingTetrahedron::MarchingTetrahedron()
{
    //ctor
}

MarchingTetrahedron::~MarchingTetrahedron()
{
    //dtor
}

irr::scene::SMeshBuffer* MarchingTetrahedron::march(DotChunk& chunk)
{
    triangleCount = 0;
    tetraCount = 0;
    distance = chunk.getDistance();
    buffer = new irr::scene::SMeshBuffer();
    Dot* cube[8];
    for (int z=0; z<chunk.getHeight()-1; z++)
    {
        for (int y=0; y<chunk.getDepth()-1; y++)
        {
            for (int x=0; x<chunk.getWidth()-1; x++)
            {
                for(int i=0; i<8; i++)
                    cube[i] = chunk.getDot(x+a2iVertexOffset[i][0],y+a2iVertexOffset[i][1],z+a2iVertexOffset[i][2]);
                marchCube(cube);
            }
        }
    }
    nlog<<"Generated "<<triangleCount<<" triangles in "<<tetraCount<<" Tetrahedrons"<<nlendl;
    return buffer;
}

void MarchingTetrahedron::marchCube(Dot* cube[8])
{
    for (int i=0; i<6; i++)
    {
        Dot* tetra[4];
        for (int t=0; t<4; t++)
            tetra[t] = cube[a2iTetrahedronsInACube[i][t]];
        marchTetrahedron(tetra);
    }
}


irr::f32 fGetOffset(irr::f32 fValue1, irr::f32 fValue2, irr::f32 fValueDesired)
{
         irr::f32 fDelta = fValue2 - fValue1;
         if(fDelta == 0.0)
         {
                 return 0.5;
         }
         return (fValueDesired - fValue1)/fDelta;
}

void MarchingTetrahedron::marchTetrahedron(Dot* tetra[4])
{
    irr::s32 iEdge, iVert0, iVert1, iEdgeFlags, iTriangle, iCorner, iVertex = 0;
    irr::u32 iFlagIndex = 0;
    irr::core::vector3df asEdgeVertex[6];
    irr::core::vector3df asEdgeNorm[6];

    for(iVertex = 0; iVertex < 4; iVertex++)
    {
        if(tetra[iVertex]->isActive())
            iFlagIndex |= (1<<iVertex);
    }
    iEdgeFlags = aiTetrahedronEdgeFlags[iFlagIndex];
    if(iEdgeFlags == 0) return;


     tetraCount++;

    //Find the point of intersection of the surface with each edge
    // Then find the normal to the surface at those points
    for(iEdge = 0; iEdge < 6; iEdge++)
    {
        //if there is an intersection on this edge
        //if(iEdgeFlags & (1<<iEdge))
        {
            iVert0 = a2iTetrahedronEdgeConnection[iEdge][0];
            iVert1 = a2iTetrahedronEdgeConnection[iEdge][1];
            //float fOffset = (tetra[iVert0]->isActive() ? -0.5 : 0.5f);//fGetOffset(tetra[iVert0]->isActive() ? 1 : -1, tetra[iVert1]->isActive() ? 1 : -1, 0);
            float fOffset = fGetOffset(tetra[iVert0]->isActive() ? 1 : 0, tetra[iVert1]->isActive() ? 1 : 0, 0.5);

            float fInvOffset = 1 - fOffset;

            asEdgeVertex[iEdge].X = fInvOffset*tetra[iVert0]->getPosition().X  +  fOffset*tetra[iVert1]->getPosition().X;
            asEdgeVertex[iEdge].Y = fInvOffset*tetra[iVert0]->getPosition().Y  +  fOffset*tetra[iVert1]->getPosition().Y;
            asEdgeVertex[iEdge].Z = fInvOffset*tetra[iVert0]->getPosition().Z  +  fOffset*tetra[iVert1]->getPosition().Z;

            //asEdgeVertex[iEdge] = tetra[iVert0]->getPosition()+(tetra[iVert1]->getPosition()-tetra[iVert0]->getPosition())*fOffset;
            //asEdgeVertex[iEdge] = !tetra[iVert0]->isActive() ? tetra[iVert0]->getPosition() : tetra[iVert1]->getPosition();

            asEdgeNorm[iEdge] = tetra[iVert0]->getNormal()+tetra[iVert1]->getNormal();
            asEdgeNorm[iEdge].normalize();
            //vGetNormal(asEdgeNorm[iEdge], asEdgeVertex[iEdge].fX, asEdgeVertex[iEdge].fY, asEdgeVertex[iEdge].fZ);
        }
    }
    //Draw the triangles that were found.  There can be up to 2 per tetrahedron
    for(iTriangle = 0; iTriangle < 2; iTriangle++)
    {
        if(a2iTetrahedronTriangles[iFlagIndex][3*iTriangle] < 0)
            break;

        triangleCount++;
        for(iCorner = 0; iCorner < 3; iCorner++)
        {
            iVertex = a2iTetrahedronTriangles[iFlagIndex][3*iTriangle+iCorner];

            buffer->Vertices.push_back(irr::video::S3DVertex(asEdgeVertex[iVertex], asEdgeNorm[iVertex], irr::video::SColor(255,255,255,255), irr::core::vector2d<irr::f32>()));
            buffer->Indices.push_back(buffer->Vertices.size()-1);
            /*
            vGetColor(sColor, asEdgeVertex[iVertex], asEdgeNorm[iVertex]);
            glColor3f(sColor.fX, sColor.fY, sColor.fZ);
            glNormal3f(asEdgeNorm[iVertex].fX,   asEdgeNorm[iVertex].fY,   asEdgeNorm[iVertex].fZ);
            glVertex3f(asEdgeVertex[iVertex].fX, asEdgeVertex[iVertex].fY, asEdgeVertex[iVertex].fZ);
            */
        }
    }
}

