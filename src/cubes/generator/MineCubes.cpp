#include "MineCubes.h"
#include "../DotChunk.h"
#include "../Dot.h"

MineCubes::MineCubes()
{
    //ctor
}

MineCubes::~MineCubes()
{
    //dtor
}

irr::scene::SMeshBuffer* MineCubes::march(DotChunk& chunk)
{
    irr::scene::SMeshBuffer* buffer = new irr::scene::SMeshBuffer();
    int cubes = 0;
    triangleCount = 0;
    for (int z=0;z<chunk.getHeight();z++)
    {
        for (int y=0;y<chunk.getDepth();y++)
        {
            for (int x=0;x<chunk.getWidth();x++)
            {
                if (!chunk.getDot(x,y,z)->isActive())
                    continue;

                hasGrass = false;

                cubes++;
                if ((z+1 < chunk.getHeight() && !chunk.getDot(x,y,z+1)->isActive())/* || z+1 >= chunk.getHeight()*/)
                {
                    generateFace(chunk.getDot(x,y,z), irr::core::vector3df(0,1,0), *buffer);
                    hasGrass = true;
                }
                if ((z-1 >= 0                && !chunk.getDot(x,y,z-1)->isActive())/* || z-1 < 0*/) generateFace(chunk.getDot(x,y,z), irr::core::vector3df(0,-1,0), *buffer);

                if ((x+1 < chunk.getWidth() && !chunk.getDot(x+1,y,z)->isActive())/*  || x+1 >= chunk.getWidth()*/) generateFace(chunk.getDot(x,y,z), irr::core::vector3df(1,0,0), *buffer);
                if ((x-1 >= 0               && !chunk.getDot(x-1,y,z)->isActive())/*  || x-1 < 0*/) generateFace(chunk.getDot(x,y,z), irr::core::vector3df(-1,0,0), *buffer);

                if ((y+1 < chunk.getDepth() && !chunk.getDot(x,y+1,z)->isActive())/*  || y+1 >= chunk.getDepth()*/) generateFace(chunk.getDot(x,y,z), irr::core::vector3df(0,0,1), *buffer);
                if ((y-1 >= 0               && !chunk.getDot(x,y-1,z)->isActive())/*  || y-1 < 0*/) generateFace(chunk.getDot(x,y,z), irr::core::vector3df(0,0,-1), *buffer);
            }
        }
    }
    buffer->recalculateBoundingBox();
    nlog<<"Generated buffer for "<<triangleCount<<" triangles with "<<buffer->Vertices.size()<<" vertices and "<<buffer->Indices.size()<<" indices for "<<cubes<<" cubes"<<nlendl;
    return buffer;
}

irr::core::vector2d<irr::f32> baseTex[] = {
    irr::core::vector2d<irr::f32>(0,0),
    irr::core::vector2d<irr::f32>(0,1),
    irr::core::vector2d<irr::f32>(1,1),
    irr::core::vector2d<irr::f32>(1,0)
};

irr::core::vector2d<irr::f32> topTex[] = {
    irr::core::vector2d<irr::f32>(0,0),
    irr::core::vector2d<irr::f32>(0.25,0),
    irr::core::vector2d<irr::f32>(0.25,0.5),
    irr::core::vector2d<irr::f32>(0,0.5)
};

irr::core::vector2d<irr::f32> downTex[] = {
    irr::core::vector2d<irr::f32>(0.25,0),
    irr::core::vector2d<irr::f32>(0.5,0),
    irr::core::vector2d<irr::f32>(0.5,0.5),
    irr::core::vector2d<irr::f32>(0.25,0.5)
};

irr::core::vector2d<irr::f32> sideTex[] = {
    irr::core::vector2d<irr::f32>(0.5,0),
    irr::core::vector2d<irr::f32>(0.75,0),
    irr::core::vector2d<irr::f32>(0.75,0.5),
    irr::core::vector2d<irr::f32>(0.5,0.5)
};

irr::core::vector2d<irr::f32>* getTexCoords(const irr::core::vector3df& normal, bool grass)
{
    if (normal.Y > 0)
    {
        return topTex;
    }
    else if (normal.Y < 0)
    {
        return downTex;
    }
    else if (grass)
    {
        return sideTex;
    }
    else
    {
        return downTex;
    }
}

void MineCubes::generateFace(Dot* dot, const irr::core::vector3df& normal, irr::scene::SMeshBuffer& buffer)
{

    irr::core::vector3df up(0,1,0);
    if (normal.Y > 0)
    {
        up = irr::core::vector3df(1,0,0);
    } else if (normal.Y < 0)
    {
        up = irr::core::vector3df(0,0,1);
    }
    irr::core::vector3df right = up.crossProduct(normal);
    right.normalize();
    up = normal.crossProduct(right);
    irr::f32 size = dot->getSize()/2.f;

    irr::core::vector2d<irr::f32>* texCoords = getTexCoords(normal, hasGrass);

    //S3DVertex(const irr::core::vector3df& pos, const irr::core::vector3df& normal, irr::video::SColor color, const irr::core::vector2d<irr::f32>& tcoords)
    buffer.Vertices.push_back(irr::video::S3DVertex(dot->getPosition()+up*size-right*size+normal*size, normal, irr::video::SColor(255,255,255,255), texCoords[0]));
    buffer.Vertices.push_back(irr::video::S3DVertex(dot->getPosition()+up*size+right*size+normal*size, normal, irr::video::SColor(255,255,255,255), texCoords[1]));
    buffer.Vertices.push_back(irr::video::S3DVertex(dot->getPosition()-up*size+right*size+normal*size, normal, irr::video::SColor(255,255,255,255), texCoords[2]));
    buffer.Vertices.push_back(irr::video::S3DVertex(dot->getPosition()-up*size-right*size+normal*size, normal, irr::video::SColor(255,255,255,255), texCoords[3]));

    buffer.Indices.push_back(buffer.Vertices.size()-2);
    buffer.Indices.push_back(buffer.Vertices.size()-3);
    buffer.Indices.push_back(buffer.Vertices.size()-4);

    buffer.Indices.push_back(buffer.Vertices.size()-1);
    buffer.Indices.push_back(buffer.Vertices.size()-2);
    buffer.Indices.push_back(buffer.Vertices.size()-4);
    triangleCount+=2;
}
