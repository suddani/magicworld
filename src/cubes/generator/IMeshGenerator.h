#ifndef IMESHGENERATOR_H
#define IMESHGENERATOR_H

#include <vector3d.h>
#include <CMeshBuffer.h>
class DotChunk;
class Dot;
class IMeshGenerator
{
    public:
        IMeshGenerator() {}
        virtual ~IMeshGenerator() {}
        virtual irr::scene::SMeshBuffer* march(DotChunk& chunk) = 0;
    protected:
    private:
};

#endif // IMESHGENERATOR_H
