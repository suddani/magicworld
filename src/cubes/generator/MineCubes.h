#ifndef MINECUBES_H
#define MINECUBES_H

#include "IMeshGenerator.h"
class MineCubes : public IMeshGenerator
{
    public:
        MineCubes();
        virtual ~MineCubes();
        irr::scene::SMeshBuffer* march(DotChunk& chunk);
    protected:
        void generateFace(Dot* dot, const irr::core::vector3df& normal, irr::scene::SMeshBuffer& buffer);
        bool hasGrass;
        int triangleCount;
    private:
};

#endif // MINECUBES_H
