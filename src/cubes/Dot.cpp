#include "Dot.h"
#include "DotChunk.h"
Dot::Dot()
{
    this->active = false;

    this->x = 0;
    this->y = 0;
    this->z = 0;
    this->chunk = NULL;
}

Dot::Dot(const irr::core::vector3df& pos_, const irr::f32& size_, int x_, int y_, int z_, DotChunk* chunk_)
{
    init(pos_, size_, x_,y_,z_,chunk_);
}

Dot::~Dot()
{
}

void Dot::init(const irr::core::vector3df& pos_, const irr::f32& size_, int x_, int y_, int z_, DotChunk* chunk_)
{
    this->pos = pos_;
    this->size = size_;
    this->active = false;

    this->x = x_;
    this->y = y_;
    this->z = z_;
    this->chunk = chunk_;
}

void Dot::init(const irr::core::vector3df& pos_, const irr::f32& size_, const bool& active_, int x_, int y_, int z_, DotChunk* chunk_)
{
    this->pos = pos_;
    this->size = size_;
    this->active = active_;

    this->x = x_;
    this->y = y_;
    this->z = z_;
    this->chunk = chunk_;
}


void Dot::setActive(const bool& active)
{
    this->active = active;
}

const bool& Dot::isActive() const
{
    return active;
}

const irr::core::vector3df& Dot::getPosition() const
{
    return pos;
}

irr::core::vector3df Dot::getNormal() const
{
    irr::core::vector3df normal;
    if (x > 0 && !chunk->getDot(x-1,y,z)->isActive()) normal.X -= 1;
    if (y > 0 && !chunk->getDot(x,y-1,z)->isActive()) normal.Z -= 1;
    if (z > 0 && !chunk->getDot(x,y,z-1)->isActive()) normal.Y -= 1;

    if (x < chunk->getWidth() && !chunk->getDot(x+1,y,z)->isActive()) normal.X += 1;
    if (y < chunk->getDepth() && !chunk->getDot(x,y+1,z)->isActive()) normal.Z += 1;
    if (z < chunk->getHeight() && !chunk->getDot(x,y,z+1)->isActive()) normal.Y += 1;


    return normal;
}

const irr::f32& Dot::getSize() const
{
    return size;
}
