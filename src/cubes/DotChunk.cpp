#include "DotChunk.h"
#include "Dot.h"
#include <libnoise/noise.h>

DotChunk::DotChunk()
{
    dots = NULL;
}

DotChunk::~DotChunk()
{
    if (dots != NULL) delete[] dots;
    dots = NULL;
}

const int& DotChunk::getWidth() const
{
    return width;
}

const int& DotChunk::getHeight() const
{
    return height;
}

const int& DotChunk::getDepth() const
{
    return depth;
}

const float& DotChunk::getDistance() const
{
    return dist;
}

Dot* DotChunk::getDot(int x, int y, int z)
{
    return &dots[z*width*depth+y*width+x];
}

void DotChunk::generateDots(int width, int height, int depth, float dist, const irr::core::vector3df& origin, bool t)
{
    //noise::module::Perlin perlin;

    this->width     = width;
    this->height    = height;
    this->depth     = depth;
    this->dist      = dist;

    if (dots != NULL) delete[] dots;

    dots = new Dot[width*height*depth];

    for (int z=0;z<height;z++)
    {
        for (int y=0;y<depth;y++)
        {
            for (int x=0;x<width;x++)
            {
                //float noise = irr::core::abs_(perlin.GetValue(1+x/width*1.f+origin.X/(39.f*0.25f)/100.f, 1+y/height*1.f+origin.Z/(39.f*0.25f)/100.f, 0.5));
                //nlog<<"Noise: "<<noise<<nlendl;
                getDot(x,y,z)->init(origin+irr::core::vector3df(x*dist, z*dist, y*dist), dist, z<30 && t, x,y,z, this);
            }
        }
    }
    changed = true;
}

bool DotChunk::change(const irr::core::vector3df& origin, const irr::f32& radius, const bool& value)
{
    for (int z=0;z<height;z++)
    {
        for (int y=0;y<depth;y++)
        {
            for (int x=0;x<width;x++)
            {
                if (getDot(x,y,z)->getPosition().getDistanceFrom(origin) < radius)
                {
                    if (getDot(x,y,z)->isActive() != value)
                    {
                        getDot(x,y,z)->setActive(value);
                        changed = true;
                    }
                }
            }
        }
    }
    return changed;
}

bool DotChunk::add(const irr::core::vector3df& origin, const irr::f32& radius)
{
    return change(origin, radius, true);
}

bool DotChunk::remove(const irr::core::vector3df& origin, const irr::f32& radius)
{
    return change(origin, radius, false);
}

const bool& DotChunk::isChanged() const
{
    return changed;
}

void DotChunk::setChanged(const bool& value)
{
    changed = value;
}

