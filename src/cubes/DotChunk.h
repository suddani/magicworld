#ifndef DOTCHUNK_H
#define DOTCHUNK_H

#include <vector>
#include <core/Tools.h>
#include <vector3d.h>

class Dot;
class DotChunk
{
    public:
        DotChunk();
        virtual ~DotChunk();

        const int& getWidth() const;
        const int& getHeight() const;
        const int& getDepth() const;
        const float& getDistance() const;

        void generateDots(int width, int height, int depth, float dist, const irr::core::vector3df& origin, bool t);

        Dot* getDot(int x, int y, int z);

        bool add(const irr::core::vector3df& origin, const irr::f32& radius);
        bool remove(const irr::core::vector3df& origin, const irr::f32& radius);
        bool change(const irr::core::vector3df& origin, const irr::f32& radius, const bool& value);
        const bool& isChanged() const;
        void setChanged(const bool& value);
    protected:
        int width;
        int height;
        int depth;
        float dist;
        Dot* dots;
        irr::core::vector3df origin;
        bool changed;
    private:
};

#endif // DOTCHUNK_H
