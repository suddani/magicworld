#include "MagicWorld.h"

int main(int argc, char* argv[])
{
    MagicWorld game(argc, argv);
    game.Init();
    game.registerProcess();
    game.Run();
    return 0;
}
