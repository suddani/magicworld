#ifndef MAGICWORLD_H
#define MAGICWORLD_H

#include <CApplication.h>

class MagicWorld : public CApplication
{
    public:
        MagicWorld(int argc, char* argv[]);
        virtual ~MagicWorld();

        void registerProcess(void);
    protected:
    private:
};

#endif // MAGICWORLD_H
